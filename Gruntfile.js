module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    'assets/css/main.css': 'assets/scss/main.scss'
                }
            }
        },
        jshint: {
            files: ['Gruntfile.js', 'assets/javascript/*.js']
        },
        watch: {
            sass: {
                files: ['assets/scss/**/*.scss'],
                tasks: ['sass']
            }
        },
    });

    // npminstall task
    grunt.registerTask('npminstall', 'Install the node dependencies', function() {
        var exec = require('child_process').exec,
        callback = this.async();

        exec('npm install', {cwd: './'}, output);
        
        function output(err, stdout, stderr) {
            if (!err) {
                grunt.log.ok('Installed');
            } else {
                grunt.log.error(stderr);
            }
            callback();
        }
    });

    // Run things up
    grunt.registerTask('start', ['npminstall', 'sass', 'jshint', 'watch']);

};
