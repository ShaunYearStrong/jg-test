(function(jQuery) {

	function updateView() {
		$.get('/api/crowdFundingPage', success);

		function success(data) {
			// basic percentage calcs
			var percentFunded = (data.totalPledged / data.target) * 100;

			// Map data to the view
			$('#jg-story').text(data.story);
			$('#jg-owner').text(data.owner);
			$('#jg-target').text(data.target);
			$('#jg-total-pledged').text(data.totalPledged);
			$('#jg-total-percent').text(percentFunded);
			// make that bar the right length
			$('#jg-bar').width(percentFunded + '%');

			// Reveal it to the world;
			$('#jg-widget').show();
		}
	}

	function showThanks() {
		$('.jg-fundraising__form').hide();
		$('.jg-fundraising__thanks').show();
	}

	function bindElements() {
		$('#jg-pledge').click(pledge);
	}

	function pledge() {
		var amount = $('#jg-pledge-amount').val();

		if (amount) {
			$.post('/api/pledge/' + amount, success);
		} else {
			$('#jg-pledge-amount').addClass("jg-fundraising__field--error");
			return false;
		}

		function success(data) {
			updateView();
			showThanks();
		}
	}

	updateView();
	bindElements();

})(jQuery);
